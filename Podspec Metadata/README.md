# SKFPulse-Util

SKF Pulse Utility contains common code used by SKF iOS apps.

## Requirements

Requires iOS version 12.0 or above.

## Installation

To get started you first need to install cocoapods on your mac

```
sudo gem install cocoapods
pod setup
```

### Within your projects `Podfile`, you will need to add the following:

At the top of the podfile, add:
```
pre_install do |installer|
    # workaround for https://github.com/CocoaPods/CocoaPods/issues/3289
    # needed for SKFPulseUtil
    Pod::Installer::Xcode::TargetValidator.send(:define_method, :verify_no_static_framework_transitive_dependencies) {}
end
```
(This is needed for `SKFPulse-Util`'s `RealmSwift` dependency)

Following that, add the source URLs for `SKFPulse-Util`'s dependencies:

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/skf/skf-cocoapods-specs.git'
```
In the body of the podfile, add the pod:
```
pod 'SKFPulse-Util', :git =>'https://bitbucket.org/skf/skfpulse-util.git', :tag => '<DESIRED-VERSION-TAG>', :modular_headers => true
```
replacing `<DESIRED-VERSION-TAG>` with that version of the pod you which to include (Example: 0.14.0).

At the bottom of the podfile, add the following:
```
# Needed for SKFAnalyticsKit dependency in SKFPulse-Util
post_install do |installer|
    installer.pods_project.targets.each do |target|
        if target.name == 'SKFPulse-Util'
            target.build_configurations.each do |config|
                config.build_settings['ENABLE_BITCODE'] = 'NO'
            end
        end
    end
end
```

(This is needed for `SKFPulse-Util`'s `SKFAnalyticsKit` dependency)

### To Complete Installation:

Once you have saved the `Podfile` you need to open a terminal window, navigate to your apps directory and run

```pod install```
