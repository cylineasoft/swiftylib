# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

NOTE: This project is currently in major version zero! 
Anything may change at any time. 
The public API should not be considered stable.

## [Unreleased]

## [0.19.0] - 2019-12-11
### Changed
- `RSRepository` now always defaults to user-based realm
- Localization changes

## [0.18.8] - 2019-12-04
### Changed
- Improved threading predictablility of `genericAPICall`. Callbacks of `genericAPICall` should now always run on the main thread.

## [0.18.7] - 2019-11-26
### Fixed
- Fixed a bug where the `pageController` of `PagerViewController` could extend off the edges of the screen if pageCount was too large. Now if more that 15 pages are assigned to a `PagerViewController`, the `pageController` will be hidden.

## [0.18.6] - 2019-11-19
### Changed
- `ISO8601DateCoder` can now decode ISO 8601 formatted dates with an arbirary number of fractional seconds digit
- `ISO8601DateCoder` now always encodes and decodes to second precision rather than millisecond precision
### Fixed
- Fixed a bug where `BlockTitleTextfield` validation messages would not display centered in the block

## [0.18.5] - 2019-11-15
### Changed
- Textfields no longer accept emoji as input
- Added maximum values to interval in `SelectionIntervalViewController`

## [0.18.4] - 2019-11-15
### Fixed
- Fixed missing localization for "Turn on access for %@ in Settings."
- Fixed a bug where `BlockTitleOnly` would be recycled with an incorrect block height.

## [0.18.3] - 2019-11-11
### Changed
- `RSRepository`'s `deleteAll()` function now has cascade deleting functionality

## [0.18.2] - 2019-11-06
### Added
- `RSRepository` can now be initialized with a user-specific realm
- `RSRepository` function `find(key:)`; provides a more efficient search method when retrieving an object via its primary key parameter
### Changed
- Simplified `DataStoreUserProfile`. User profiles are set to always be saved in default realm
### Fixed
- Fixed a bug where the string representation of a date would have the wrong year for late-December dates
- Fixed a bug in the `deleteAll()` function of `RSRepository` where it would delete all objects of all types, rather than only the type defined for the repository
### Removed
- Removed `DebugMessages` class

## [0.18.1] - 2019-10-25
### Fixed
- Fixed a bug where `BlockTitlePhotoSelect` alert would cause screen to become unresponsive on iPad.
- Fixed a bug where `AlertTextViewController` would disappear immediately after being displayed.
### Removed
- Removed `AlertBuilder` support for `.actionSheet` alert style

## [0.18.0] - 2019-10-22
### Added 
- Added `AlertBuilder` Class. Use for building iOS13 compatable alerts
### Fixed
- Fixed a bug in iOS13 where alerts would disappear immediately after being displayed
- Fixed a bug in `BlockTextview` where the last input to the textview was not being sent in `onTextviewValueChanged(newText:)`
- Fixed a bug in `BlockTitlePhotoSelect`where camera/photo-library was being opened from a background thread, causing a delay in the opening of the camera/gallery.
### Removed
- Removed `AlertFactory` Class

## [0.17.10] - 2019-10-16
### Added
- Added `ISO8601DateCoder` which can be used to encode and decode dates in ISO8601 format
### Deprecated
- Deprecated `utcDateFormatter`
- Deprecated `utcDateFormatterWithoutMS`
### Fixed
- Fixed a bug where modal view controllers would show in card view rather than fullscreen on iOS13 when compiled using XCode 11
- Fixed a bug where the icon of `BlockIconTitles` would become distorted on large screens
- Fixed a bug where `BlockTitleTextfield_WithSelectionAndImage` would display oddly when no image is selected

## [0.17.9] - 2019-10-02
### Changed
- Updated RealmSwift to version 3.18.0
- SyncQ with id parameter passed will now insert it in the appropriate place anywhere within the API URL
### Fixed
- Fixed a localization error for the copyright notice on the login screen

## [0.17.8] - 2019-09-27
### Added
- Added sync icon
- Added ability to upload syncQ objects with API requiring ID in URL

## [0.17.7] - 2019-09-25
### Added
- Added encoding and decoding functions for `T_RepeatFrequency`
### Fixed
- Fixed some errors in localization

## [0.17.6] - 2019-08-20
### Added
- Added Chinese localization
- Added check for valid internet connection after an alamofire failure.
### Changed
- Changed how BlockSquareImage calculates its height
### Fixed
- Fixed a bug where SKF Banner on Login Screen would become stretched

## [0.17.5] - 2019-07-12
### Added
- Added server indicator to version string
- Added release url to list of server targets
### Fixed
- Fixed a bug where default `cGood_Green` color was actually orange.

## [0.17.4] - 2019-07-09
### Added
- Added autofill for Country in ProfileViewController

## [0.17.3] - 2019-07-02
### Added
- Added ability to define title for SelectionIntervalViewController
### Fixed
- Fixed missing localized string for "Privacy Policy"
-Fixed a bug where textfield entries would be saved with non-breaking spaces, preventing word wrapping

## [0.17.2] - 2019-06-12
### Fixed
- Fixed a bug where `BlockTitlePhotoSelect` would not correctly display the selected image when viewed fullscreen

## [0.17.1] - 2019-06-11
### Added
- Added `displayRatio` parameter to BlockTitlesDetail to define the space alotted to the Title/Subtitle versus the space alotted to the detail text
### Fixed
- Fixed a missing reference to `UserDataStoreProtocol` in the project file

## [0.17.0] - 2019-06-10
### Added
- Spanish localization
### Changed
- UI now adapts better to longer strings and larger font sizes
### Fixed
- Fixed a bug where some API calls would incorrectly check the refresh token


## [0.16.1] - 2019-06-03
### Added
- Cells with textfields now have a `doesWholeCellSetFocus` attribute, which when set to true allows selecting any part of the cell to select the textfield.

## [0.16.0] - 2019-05-27
### Added
- Added handling for `ExpiredCodeException` to `PostForgotPasswordComplete` API
- Added internet connection check to APIs
- Added ability for `NetworkRequest` mockups to simulate connection to internet (or lack thereof)
- Added more unit tests for API code
### Changed
- Most APIs have been refactored to use the `genericAPICall` function
- `postRefreshToken` API callbacks have been simplified
- `isUnauthorized` is now an extension of `DataResponse`
- Updated to Swift 5
### Fixed
- Fixed unsafe unwrapping of `profileModel` in `postLogin` API
### Removed
- Removed unused `onRequireLogout` calls from APIs
- Removed checks for `ExpiredCodeException` in APIs that would never occur
- Removed generic `ErrorCode` enum; replaced with specific errors listed within each API

## [0.15.14] - 2019-05-17
### Added
- Added `isButtonEnabled` and associated `update()` function to cells which include buttons
- Added Spanish localization
### Changed
- Changed notification badge default from red dot to blue square
### Fixed
- Fixed a bug where profile ID wasn't being decoded on login

## [0.15.13] - 2019-05-10
### Added
- Added AlertFactory to display standard alerts
- Added workaround for users verifying email using the wrong password
- Added workaround for users attempting to change password without verifiying email
- Added function for tinting animated `UIImageView`s
### Changed
- Changed order of password criteria when creating a new password
### Fixed
- Fixed unprocessed error messages from the backend
- Fixed bug where user may be able to proceed past Create an Account page although email entered is already attached to an existing account
- Fixed some memory leaks
- Fixed a bug where attempting to add a space to the middle a textField, adds it to the end of the textField
- Fixed a crash that could occur when a user logs out while syncing with the backend
### Removed
- AlertUtil showErrorAlert function (functionality covered by AlertFactory)

## [0.15.12] - 2019-04-29
### Added
- Added ability to remove messages from syncq based on returned error message
- Added ability to customize dimensions of `BaseAlertViewController`
- Added ability to add notification badge to cells.
### Changes
- Simplified display logic for subclasses of `BaseAlertViewController`
- Changed handling of UPPERCASED header titles
### Fixed
- Fixed a bug where `BaseAlertViewController` would cause the status bar to change color
- Fixed issue with `diffInDays` method not returning the correct number of days because the time difference between the dates is less than 24 hours. 

## [0.15.11] - 2019-04-23
### Fixed
- Fixed a bug where buttons within a header could cause a crash

## [0.15.10] - 2019-04-22
### Fixed
- Fixed a bug where hitting cancel while in the Confirm Pin screen would not return to the login screen.

## [0.15.9] - 2019-04-17
### Changed
- Changed method of defining constraints of BaseView to improve behaviour when using Dynamic Type

## [0.15.8] - 2019-04-12
### Fixed
- Fixed a improper call to `countsyncQ` that prevented the pod from compiling

## [0.15.7] - 2019-04-12
### Changed
- Added capture lists to closures to prevent memory leaks due to strong reference cycles
- Made API generic error responses public
### Fixed
- Added handling for some missing cases on posting a refresh token
### Removed
- Removed sync item function from API call

## [0.15.6] - 2019-04-10
### Added
- Added optional `timeout` parameter for AlertUtil's `busy()` function. The timeout indicates the number of seconds the busy indicator should wait before dismissing itself.
### Changed
- Pulse checks are deleted from the queue if there is already a pulse check pending (Note: This should eventually be moved into the Pulse App and out of the Util pod)
- Updated Dev server URL

## [0.15.5] - 2019-04-05
### Added
- Added function for removing change and insertion notifications for real object (needed for fetching and creating new assets to avoid reposting of data)
- Changed the tokens structure saved when notifications added to realm objects
- Added ability to define custom message for `AlertUtil`'s `busy()` loading indicator.
### Changed
- Password requirement display now updates instantaneously
### Fixed
- Fixed a bug where placeholder font size would increase when showing a red placeholder text warning
- Fixed a bug where reloading of a table view could cause some cells to display incorrectly
- Fixed a bug where attributes of an attributed string would be overwritten for section header blocks
- Fixed a bug where cell separator lines would become hidden unintentionally

## [0.15.4] - 2019-03-28
### Added
- Added generic response class
- Added error information to sync queue item
- Added new sync queue state when not synced
### Changed
- Cells now adopt the state of their section's `allowRowMove` parameter
### Fixed
- Updated tests for the API generic calls

## [0.15.3] - 2019-03-27
### Added
- Added `logPageView` closure to `BaseViewController` to allow custom definition of page-logging behaviour
- Added `ErrorLogger` utility to allow custom definition of API error-logging behaviour
### Removed
- Removed SKFAnalyticsKit and dependencies (crashing the app when deploying to test flight)

## [0.15.2] - 2019-03-23
### Added
- BlockTitleTextfield now has an `update(minRange:)` and `update(maxRange:)` function
- Intervals chosen by IntervalSelectionViewController are restricted to a maximum of 10 digits. 
- When selecting an interval with a unit of hours, the interval is restricted to a minimum of 24 hours
- Added `maximumDate` and `minimumDate` parameters to `BlockTitleTextfieldWithDatePicker`
### Changed
- IntervalSelectionViewController now uses a textfield rather than a picker for selection of interval value
- Loading indicator changed to SKF bearing loading indicator
- Plant/Facility field of user profile is now optional
### Fixed
- Fixed a bug where "week" would not properly pluralize
- Fixed a bug where only unedited photo would be saved when selecting a photo from camera or gallery
### Removed
- Removed `allowFutureDates` of `BlockTitleTextfieldWithDatePicker`. Replace with `maximumDate`.

## [0.15.1] - 2019-03-22
### Changed
- "Accept Terms and Conditions" message now displays appropriate app name
### Fixed
- Fixed a bug where BaseAlertViewController would display incorrectly

## [0.15.0] - 2019-03-22
### Added
- Extended Realm to allow for cascading deletes
- Added Contains method to the Query object
- Generic post API unit tests
### Changed
- Removed "SKF" from the beginning of the app name on the login screen
- Changed timing for when tableViews are initialized within the base ViewControllers

## [0.14.7] - 2019-03-19
### Fixed
- Fixed a bug where status bar would remain hidden indefinitely
- Fixed a crash when loading the Create Account screen

## [0.14.6] - 2019-03-19
### Changed
- Allow override of `tableView(numberOfRowsInSection:)` for hiding of rows
### Fixed
- Fixed a crash that could occur when scrolling a table while the keyboard is open

## [0.14.5] - 2019-03-19
### Fixed
- Fixed a bug where `BaseWithPagerViewController` would create multiple instances of its `PagerViewController`

## [0.14.4] - 2019-03-19
### Fixed
- Fixed a crash where `BaseWithPagerViewController` would attempt to access its table view before it was initialized. 

## [0.14.3] - 2019-03-19
### Fixed
- `BaseViewController` now subclasses `CustomStatusBarViewController` to improve status bar style consistency 

## [0.14.2] - 2019-03-19
### Added
- Added `pushViewController` and `popViewController` completion handlers
### Changed
- `buttonImage` property of `ViewButton` is now optional
- added `update(leftButtonImage:)` and `update(rightButtonImage:)` to `BlockButtonTitlesButton`
- Copywrite year now generated programatically
- Added `CustomStatusBarViewController`, which sets its status bar style based on the `statusBar_Style` attribute of `VisualTheme`
### Fixed
- Fixed processing sync queue when access token has expired
- Fixed a bug where a portion of the screen would be hidden behind the tab bar
- Fixed a bug where `SelectionIntervalViewController` would display red text instead of grey

## [0.14.1] - 2019-03-15
### Changed
- SKFLog is now automatically called during `viewDidAppear` of any view controller that subclasses `BaseViewController`
- Changed the way PagerViewController returns its `onPageDidScroll` callback
- Cleaned up podspec file
### Fixed
- Fixed postAssetAttachment API not finalizing properly on success

## [0.14.0] - 2019-03-12
### Added
- Added SKFAnalyticsKit pod.  **NOTE:** This static library needed special settings in podspec and in the calling pofile (config.build_settings['ENABLE_BITCODE'] = 'NO')
- Added wrapper for login errors and view controllers
- Added logs for all api errors and opening of shared view controllers

### Fixed
- Fixed a crash caused by opening the photo selection screen while the keyboard was on screen
- Textfield range warning messages now display an appropriate number of decimal points

## [0.13.11] - 2019-03-11
### Fixed
- BlockRoundImage will no longer crash if given a nil image

## [0.13.10] - 2019-03-11
### Added
- Added a date formatter option for dates not including milliseconds
### Changed
- Textfield range error messages now only display 1 decimal place
- Swapped the Terms of Use and Privacy Policy tabs
### Fixed
- Fixed a bug in the localization of textfield range error messages


## [0.13.9] - 2019-03-07
### Fixed
- Fixed a bug where profile would not update after being saved
- Fixed a bug where industries list appeared empty
- An expired access token now updates when a generic, password, or user profile api call is made
- Fixes an issue where backend would sometimes send a lowercase GUID
- Fixes an issue with ViewButton imageView tintColor not using the same tintColor set on the button

## [0.13.8] - 2019-03-05
### Added
- Added a callback for when internet connection is restored (. New feature from ios 12).  The calback processes all items in queue
- Added connection flag
### Changed
- Added localized error messages for invalid input
### Fixed
- Fixed a bug where no alert appears after changing password successfully
- Fixed a bug where custom validation is not performed when minRange, maxRange, allowableDecimalPlaces are all not specified
- Fixed a bug where app crashes when attempting to validate a field that only has either a minRange or maxRange, but not the other
- Fixed a bug where passwords must match does not turn red when one field is edited (is nil) and the other field is not edited (is empty string)
- Permission alert for camera access now displays correct app name
- Added a second sort descriptor to avoid changing order of transmission between create asset and edit asset
- Fixed post of asset attachement which was not increasing the attempt variable

## [0.13.7] - 2019-03-04
### Changed
- `getUser(guid:)` now uses case-insensitive compare
### Fixed
- Added some missing due date localization 
- Fixed a bug where status bar would appear when displaying an alert

## [0.13.6] - 2019-03-04
### Added
- Retrial timer for failed TX objects after 1 hour
- Access to the queue available to be processed from the app
- Added sync indicator (indicates when queue is empty)
- Added RxSwift pod dependancy
- Added automatic resizing of title textfields
### Fixed
- Fixed missing localized string in Create Account screen
- Fixed a bug where having a nil value for showCellSeparatorLine caused app to crash



## [0.13.5] - 2019-02-28
### Changed
- Changed how title height for welcome page is set
### Fixed
- Fixed several incorrect view controller dismissal calls
- Fixed a bug where Other would not correctly display for a SelectionViewController

## [0.13.4] - 2019-02-28
### Added
- Added ability to reload rows and section headers within the top table view of a BaseWithTopViewController
- Added ability to call SelectionIntervalViewController modally
### Changed
- Replaced dummy headers of block height 0 in shared screens with addEmptyHeaderSection
- Make API Server's URL configurable based on the current scheme. 

## [0.13.3] - 2019-02-27
### Changed
- Server changed to production

## [0.13.2] - 2019-02-27
### Changed
- Confirmation Pin renamed to Verification Code
- Updated the UI of several authentication screens
### Fixed 
- Profile not read properly because of uppercase bug
- Fixed a bug where textfield validation error messages would overlap
- Fixed a bug where the incorrect alert message would display when entering an invalid old password while changing passwords
- Fixed a bug where email alert would appear when
- Verification Code now uses numeric keyboard instead of alphanumeric keyboard

## [0.13.1] - 2019-02-27
### Changed
- Changed encoding strategy for Double.NaNs from "0' to "NaN"
### Fixed 
- Items are removed from TX queue after 5 tries and from sorted queue based on number of TX attempts

## [0.13.0] - 2019-02-27
### Changed
- Changed initializer for ConfigTitleTextfield_WithSelectionAndImage; You no longer directly define BlockHeight. Instead, use parameters `imageWidth` and `imageAspectRatio`, which the block can then use to adjust itself to the proper size. Also added parameter `blockHeightWithNoImage` which can be used to adjust the height of the block when the block has no image.
- Changed the way ConfigTitleTextfield_WithSelectionAndImage adjusts block height so that you do not need to call extra functions from within the ViewController during use


## [0.12.7] - 2019-02-26
### Changed
- Removed uppercase constraint on passwords
### Fixed 
- Fixed a bug cause by BlockScrollviewonly's xib file being incorrectly labeled
- On textField:shouldChangeCharactersInRange, invoke the client's onTextFieldValueChanged callback if textfield is empty before exiting

### Added 
- Added length constraint on the isValidPassword method. 

## [0.12.6] - 2019-02-26
### Fixed
- Fixed a bug where block _WithSelectionAndImage would not properly resize when updated
- Fixed a bug inLoginForgotPasswordCompleteViewController where invalid textfield labels were not shown in red when confirm button was pressed

## [0.12.5] - 2019-02-26
### Added
- Added mandatory methods `setupTopTableViewHeight()` and `setupPages()` to BaseWithPagerViewController to enforce that the definition of table height and page views are made at the appropriate times.
### Fixed 
- Fix a bug that locked up the queue when transmitting an image due to a reachability flag change.  
### Removed
- Removed unused flags `haveDeterminedReachability` and `sessionIsOnline`

## [0.12.4] - 2019-02-25
### Fixed 
- Input should now be properly validated when a user is trying to change their password. 

## [0.12.3] - 2019-02-25
### Added
- Added displayError function for displaying NSURLErrorDomain and Alamofire.AFErrors thrown when making webservice calls 
- Added displayAPIError function for displaying SKF's server error

## [0.12.2] - 2019-02-25
### Changed
- EditProfileViewController now uses the `pageState` enum to define its state (Add or Edit) rather than the `editProfile` variable.
- `rdcEmail` in DataServer is now public
### Deprecated
- Deprecated `editProfile` variable of EditProfileViewController. Use `pageState` to define the view controller's state instead.
### Fixed
- renamed `BaseViewWithPagerController` to `BaseWithPagerViewController`


## [0.12.1] - 2019-02-25
### Added
- Added BaseWithPagerViewController and PagerViewController for creating screens with page-style navigation
- Added ButtonTitlesButton Block
- Added `update(blockHeight:)` function to BaseBlock
- Added convenience functions `addEmptyHeaderSection()` and `addEmptyCell()` to BaseTableView, used for creating dummy sections and cells within a tableview
- Added SelectionIntervalViewController, used to display a detailed time interval selection screen
### Changed
- Changed API Servers to secure HTTPS URLs
- Textfields no longer accept strings which are entirely whitespace.


## [0.12.0] - 2019-02-22
### Changed
- Some API calls have been changed
- Industries are now listed in alphabetical order, with "Other" at the bottom
### Fixed
- Fixed several formatting issues on authentication screens
- Fixed bugs that could prevent a user from logging in
- Fixed a bug where applications list would be empty if user had "Other" as their industry
- User must now accept Terms & Conditions before a confirmation pin will be sent to their email
- Removed check for authentication token during user registration
- Fixed network errors that caused slowdowns in the app


## [0.11.6] - 2019-02-22
### Added
- Added `displayFrom(id:)` function for PickableEnum
- Added an optional callback to the API TX queue
- Added post of images to the API TX queue
- Added PostAssetAttachment API function
- Added BaseAlertViewController
- Added ButtonTitleButton base block
- Added AlertTextviewViewController, an alert popup that contains a UITextview
### Deprecated
- Deprecated `fromID(id:)` function for PickableEnum. renamed to `enumFrom(id:)`
### Changed
- Changed TX queue to allow images to be posted under the same tx process 
### Fixed
- "skf_last_login_guid" keys are now stored as uppercase to avoid case sensitive changes from backend
- Fixed a crash that occured when several items are posted quickily by Tx Queue to the API. 

## [0.11.5] - 2019-02-20
### Added
- Added `validateSelf()` function to BlockTitleTextfield to display edit check warnings without reloading a row

## [0.11.4] - 2019-02-19
### Changed
- Changed access control for base views and blocks to allow for app-specific block creation
### Fixed
- Fixed cell registration logic to allow for registering of Nibs from main bundle
- Fixed misalignment of button for BlockTitlePhotoselect 
### Removed
- Removed extra ellipsis from loading indicator message

## [0.11.3] - 2019-02-19
### Added
- `addSpacing(spacing:)` function to BaseTableView for use when creating clear spacer cells 
### Fixed
- Added checks to RSRepository to avoid writing while already in a write transaction
- Text in some cells will now scale down font size instead of truncating long strings

## [0.11.2] - 2019-02-18
### Added
- BlockButtonSingle - new attributes - `buttonPaddingLeft` and `buttonPaddingRight`
- BlockTextfiled - new attribute - `spacesNotAllowed` - ability to prevent spaces from being entered (defaulted to true for WithUnitsOfMeasure)
### Fixed
- SectionHeaderTitle - Incorrect size - font changed from fReg to fSmall
- BlockTitlePhotoselect - Incorrect aspect ratio for picture - now scaleAspectFit
- Textfield should not contain whitespace - field can not contain just whitespaces or leading spaces
- Change password - prevent user from entering new password the same as the previous password
- LoginVC does not display proper app name
- Please wait alert localized

## [0.11.1] - 2019-02-15
### Added
- Added access to the sync queue without the need of realm notifications
- Added debugging code for backend

### Removed
- Removed default test account

## [0.11.0] - 2019-02-15
### Added
- Realm Repository: create RSRepository and RSQuery classes to handle all Realm functionality
### Removed
- Realm `writeWithBlock`
- Picker block `updateFieldWithValue`
- SelectionVC `showVC_Modal_PickableEnum`
- SelectionVC `showVC_Push_PickableEnum`

## [0.10.11] - 2019-02-15
### Added
- Added `customValidation` callback to textfield cells. Can be used to define custom requirements for a cell's textfield.

## [0.10.10] - 2019-02-15
### Fixed
- Improved alerts and localization for confirmation pin screen.
- Fixed a bug where cell separators would sometimes appear even when `showCellSeparatorLines` was set to false
- Fixed layout of Forgot/Change Password Screen
- Fixed layout of Create Account Screen
- Done button on keyboard now dismisses the keyboard    
- Text will now automatically resize when user changes text size via settings on their device; you will not have to close the app

## [0.10.9] - 2019-02-14
### Added
- SelectionHelpViewController
### Changed
- Improvements to Authentication Localization

## [0.10.8] - 2019-02-14
### Added
- Added check for empty profile and invalid industry when refreshing access token.
### Fixed
- Fixed a crash when clicking "Resend Pin" on the Pin Confirmation screen.
- Fixed a crash when attempting to select a photo (iPad only).
- Fixed bug where status bar text would display as black instead of white.

## [0.10.7] - 2019-02-13
### Added
- BlocktitleTitleTextfield will now show a message indicating the allowable range when an invalid value is input into a textfield.
- Added overloads `showVC_Push(pickableEnumType:)` and `showVC_Modal(pickableEnumType:)` in SelectionViewController
### Changed
- PickableEnum variables related to SelectionVC handling are now static instead of instance members.
### Deprecated
- Deprecated `showVC_Push_PickableEnum` and `showVC_Modal_PickableEnum` in SelectionViewController. Use `showVC_Push(pickableEnumType:)` and `showVC_Modal(pickableEnumType:)` instead.


## [0.10.6] - 2019-02-13
### Added
- Added access to the syncService tokens array.
- Added default strings for "Double" special cases when serializing to the backend.
### Changed
- Changed profile GUID to uppercase (to avoid breaks if backend format changes).
### Removed
- Removed callbacks when an object is deleted from the realm database.

## [0.10.5] - 2019-02-12
### Fixed
- Fixed a bug where a table view would bounce whenever a row was selected.
- Fixed a bug where `tableView(canEditRowAt:)` would always return true.
- Removed a call to depreciated function `updateFieldWithValue(value1:value2:)` 
- Range edit check - when more than 3 decimal places ; now limits number of decimal places entered based on `allowableDecimalPoints` property

## [0.10.4] - 2019-02-11
### Added
- Added support for Dynamic Type

## [0.10.3] - 2019-02-11
### Added
- SelectionVC can now be called passing a PickableEnum instead of an array of Strings.
- A PickableEnum may now specify how SelectionVC should handle selections of that type, including allowing of other, none, as well as whether a help view should be displayed.
- Added `update(cellAccessoryType)` function to BaseBlock
### Changed
- Keyboard now uses UIReturnKeyType.done instead of addDoneButton
### Deprecated
- `updateFieldWithValue(value1:value2:)` in BlockTitleTextField_WithPicker. Replaced with `update(value1:value2:)`
### Fixed
- Fixed a bug where picker would not display previously selected value.
- Fixed a bug where clicking "Done" without changing a picker's value would select the wrong entry.

## [0.10.2] - 2019-02-08
### Added
- This CHANGELOG.md 
### Fixed
- Fixed a bug where Terms of Use and Privacy Policy could not be scrolled all the way to the bottom



