# To submit changes to the SKFPulse-Util Cocoapod:

## First-Time Setup:
1. Clone the pod's repository into the `~/Documents/Libraries/` working directory on your computer

## Setting up a local version of the pod:
1. Within the development project that includes `SKFPulse-Util`, open the `Podfile`
2. Comment out (`#`) any instance of the pod reference `'SKFPulse-Util', :git =>...`
3. For each of the above instances, replace a reference to your local version of the pod: 
```pod 'SKFPulse-Util', :path => '~/Documents/Libraries/SKFPulse-Util'```
4. In terminal, navigate to your project's working directory and run `$ pod update SKFPulse-Util`
5. Verify that within your development project workspace, in the `Pods` folder, you now see a folder named `Development Pods`.
6. Change the build system of XCode:
- In the XCode workspace of your development project, navigate to `File -> Project/Workspace Settings`.
- Under `Shared Workspace Settings -> Build System`, change the build system to `Legacy Build System`
- This change will allow updates you make to the pod to be immediately visible when the project is built, without the need to run `pod update` after every change

## Developing with the pod:

You are now free to make changes to the local version of the podfile. 

When pushing any changes to the repository, it is suggested you create a new branch, named with your initials and a description of the changes being made.

## Submitting a change to the SKFPulse-Util Cocoapod:

One you are ready to submit your changes, you should do the following:


1. Update the `CHANGELOG.md` file in the pod, adding notes of the changes made to the [Unreleased] section. See [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) for a guide on changelog formatting.
2. Push those changes to the remote repository.
3. Submit a pull request, including a description of what changes have been made.


Don't forget to change your reference in the `podfile` of the development project back to the remote repository `'SKFPulse-Util', :git =>...`  when you are done!



# Localization

Last Updated: 2020-01-28

## Adding new strings to your project
Lets say we have a new string `"Hello World"` we would like to add to the project. We would perform the following steps:
1. Decide on a key for the string. The key should consist of only lowercase letters, numbers, and underscores. The key should start with a prefix (consisting of only lowercase letters) which describes the context for the string. For the example, we will choose a key of `example_hello_world`
2. Add the string to `L10n-Temp` as a `static let` extension of `L10n`. The variable name should be a camelCase version of the key defined in step 1. In the example case, the variable name will be `exampleHelloWorld`.  Assign the english string literal (`"Hello World"`) as the value of the variable. `L10n-Temp` should now look like this:
```
extension L10n {
    static let exampleHelloWorld = "Hello World"
}
```
3. Reference `L10n` wherever you need your string within the code. 
```
print(L10n.exampleHelloWorld)
```

## Adding your new strings to Lokalise
For addition of a small number of strings, use the "Add key" functionality of Localize, which you can find on the Lokalise project dashboard. 
* `Key` should be in the same format defined in the previous section (ex. `example_hello_world`)
* `Base language value` should be the english string literal (ex. `"Hello World"`)
* Under most circumstances, `Platforms` should include `iOS` and `Android`
* Add a description if extra explaination is needed to understand the context of the string
* At minimum, `Tags` should include the tag defining the xcode-project for the string. For example, strings added to this project should be tagged with the `shared` tag. A string added to Pulse should be tagged with the `pulse` tag.
* Extra tags may be added to categorize a string, if necessary. For instance, a group of strings consisting of a list of countries could all be tagged with the `country` tag.

## Exporting from Lokalise
After you've added your new strings to Lokalise (and had them translated), you'll need to bring them back into the project:
1. Navigate to the `Download` section of your project in Lokalise
2. Set the format to `Apple Strings (.strings, .stringsdict)`
3. Under `File Structure`, select `All keys to a single file per language` with a file structure of `%LANG_ISO%.lproj/Localizable.%FORMAT%`
4. Under `Include Tags`, select the tag for the xcode-project you are updating. This will ensure you do not end up with duplicates between specific projects and the shared code.
5. Under `Empty Translations`, select `Replace with base language`. This will allow the app to default to english if any translations are missing. If you are specifically planning to test the translation coverage of the app, you may wish to instead set this field to `Don't export`, which will make it easier to spot missing translations.
6. Under `Plural Format`, select `Default for Apple Strings`
7. Under `Placeholder Format`, select `iOS`
8. Under `Indentation`, select `2 spaces`
9. Under `Options`, all fields should be unchecked
10. Under `Data to export`, check `All`
11. Click `Build and download` to download your updated Localization files.
12. Replace the files in the `LOCALIZATION` folder of the xcode project with the new downloaded files.
13. In terminal, navigate to the root folder of the xcode project and run the command `$ swiftgen`. This will update the `L10n-Generated` file for the project.
14. Remove any variables previously added in `L10n-Temp`. Your previous references pointing to `L10n-Temp` should now autmatically find their references in `L10n-Generated`

## Plurals
Pluralized strings may be created in Lokalise via the `Plural` switch in the `Advanced` tab of the Key editor. Turning on pluralization will allow you to individually define different forms of the string based on the value of the parameter passed in. Generally, pluralized strings should pass in one integer parameter. The placeholder for this parameter in Lokalise is `[%i]`. As an example, localizing the phrase "I have _ apples." might look as follows in Lokalise:
```
ZERO: I have no apples.
ONE: I have 1 apple.
OTHER: I have [%i] apples.
```
When downloading plural strings, they will be placed in their own `Localized.stringsdict` file. Unfortunately, `SwiftGen` cannot parse `.stringsdict` files. Consequently, we must manually define the `L10n` functions for any new plural strings by adding them to the `L10n-Plurals` file. For the example above, the following lines should be added to `L10n-Plurals`:
```
/// I have %li apple(s).
static func exampleIHaveXApples(_ p1: Int) -> String {
    return L10nHelper.tr( "example_i_have_x_apples", p1)
}
```

