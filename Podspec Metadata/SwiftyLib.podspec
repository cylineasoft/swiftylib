Pod::Spec.new do |s|
  s.name         = "SwiftyLib"
  s.version      = "0.0.1"
  s.summary      = "A short description of SwiftyLib."

  s.description  = <<-DESC
  This CocoaPods library helps you perform calculation.
                   DESC

  s.homepage     = "https://bitbucket.org/cylineasoft/SwiftyLib"
  s.license          = { :type => 'Private', :file => 'LICENSE' }
  s.author             = { "pdumonti" => "pdumonti@cylinea.com" }
  
  s.source       = { :git => "https://bitbucket.org/cylineasoft/swiftylib.git", :tag => s.version.to_s }

  s.ios.deployment_target = '12.0'
  s.swift_version = '5.0'
  s.source_files = 'SwiftyLib/Classes/**/*'
  #s.source_files = 'SKFPulse-Util/Classes/**/*'

  #s.source_files  = "Classes", "Classes/**/*.{h,m}"
  #s.exclude_files = "Classes/Exclude"

  

end
