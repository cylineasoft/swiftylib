//
//  SwiftyLib.swift
//  SwiftyLib
//
//  Created by Patricia Dumontier on 2020-06-18.
//  Copyright © 2020 Patricia Dumontier. All rights reserved.
//



public final class SwiftyLib {

    let name = "SwiftyLib"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
