//
//  SwiftyLib.h
//  SwiftyLib
//
//  Created by Patricia Dumontier on 2020-06-18.
//  Copyright © 2020 Patricia Dumontier. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for SwiftyLib.
FOUNDATION_EXPORT double SwiftyLibVersionNumber;

//! Project version string for SwiftyLib.
FOUNDATION_EXPORT const unsigned char SwiftyLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftyLib/PublicHeader.h>


